package com.brian.assessment.rest;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.brian.assessment.exceptions.TradeNotFoundException;
import com.brian.assessment.rest.DefaultExceptionHandler;

public class DefaultExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(DefaultExceptionHandler.class);
    
    @ExceptionHandler(value = {TradeNotFoundException.class})
    public ResponseEntity<Object> TradeNotFoundExceptionHandler(
            HttpServletRequest request, TradeNotFoundException ex) {
        logger.warn(ex.toString());
        return new ResponseEntity<>("{\"message\": \"" + ex.getMessage() + "\"}",
                                    HttpStatus.NOT_FOUND);
    }
}