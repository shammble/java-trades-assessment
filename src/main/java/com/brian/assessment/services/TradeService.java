package com.brian.assessment.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.brian.assessment.dao.TradeDao;
import com.brian.assessment.model.Trade;

@Component
public class TradeService {
	@Autowired
	private TradeDao tradeService;
	
    public List<Trade> findAll(){
    	return tradeService.findAll();
    }

    public Trade findById(int id){
    	return tradeService.findById(id);
    }

    public Trade create(Trade trade)
    {
    	if(trade.getStock().length()>0)
    	{
    	return tradeService.create(trade);
    	}
    	throw new RuntimeException("Invalid Parameter: stock traded:"+trade.getStock());
    }

    public void deleteById(int id)
    {
    	tradeService.deleteById(id);
    }
}
