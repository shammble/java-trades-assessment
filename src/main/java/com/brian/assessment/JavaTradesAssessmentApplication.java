package com.brian.assessment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaTradesAssessmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaTradesAssessmentApplication.class, args);
	}

}
