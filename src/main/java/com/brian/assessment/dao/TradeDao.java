package com.brian.assessment.dao;

import java.util.List;

import com.brian.assessment.model.Trade;


public interface TradeDao {

    List<Trade> findAll();

    Trade findById(int id);

    Trade create(Trade trade);

    void deleteById(int id);
}
