package com.brian.assessment.model;

public class Trade {

	private int id;
	private String stock;
	private double price;
	private int volume;
	
	
	//constructor
	public Trade(int id, String stock, double price, int volume) {
		this.id = id;
		this.stock = stock;
		this.price = price;
		this.volume = volume;
	}
	//getters
	public int getId() {
		return id;
	}
	public String getStock() {
		return stock;
	}
	public double getPrice() {
		return price;
	}
	public int getVolume() {
		return volume;
	}
	
	//setters
	public void setId(int id) {
		this.id = id;
	}
	public void setStock(String stock) {
		this.stock = stock;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public void setVolume(int volume) {
		this.volume = volume;
	}
	@Override
	public String toString() {
		return "Trade details: [id=" + id + ", stock=" + stock + ", price=" + price + ", volume=" + volume + "]";
	}
	
	

	
	
}
