package com.brian.assessment.exceptions;

@SuppressWarnings("serial")
public class TradeNotFoundException extends RuntimeException {

	public TradeNotFoundException(String msg) {
		super(msg);
	}
	
}
