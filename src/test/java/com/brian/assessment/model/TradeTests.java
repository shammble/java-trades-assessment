package com.brian.assessment.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;


public class TradeTests {

	private int testId =37;
	private String testStock="BRB";
	private double testPrice=37.00;
	private int testVolume=100;
	
	@Test
	public void test_Trade_constructor()
	{
		Trade testTrade= new Trade(testId,testStock,testPrice,testVolume);
		assertEquals(testId, testTrade.getId());
        assertEquals(testStock, testTrade.getStock());
        assertEquals(testPrice, testTrade.getPrice(), 0.0001);
        assertEquals(testVolume, testTrade.getVolume(), 0.0001);
        
	}
	@Test
    public void test_Employee_toString() {
        String testString = new Trade(testId,testStock,testPrice,testVolume).toString();
        assertTrue(testString.contains((new Integer(testId)).toString()));
        assertTrue(testString.contains(testStock));
        assertTrue(testString.contains(String.valueOf(testPrice)));
        assertTrue(testString.contains(String.valueOf(testVolume)));
    }
	
}
