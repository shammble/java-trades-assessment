package com.brian.assessment.dao;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.brian.assessment.exceptions.TradeNotFoundException;
import com.brian.assessment.model.Trade;


public class InMemTradeDaoTests {
	private static Logger LOG = LoggerFactory.getLogger(InMemTradeDaoTests.class);
    private String testName = "BRB";
    private double testPrice = 37.00;
    private int testVolume = 100;
    
    @Test
    public void test_saveAndGetTrade() {
    	Trade testTrade=new Trade(-1,testName,testPrice,testVolume);
    	InMemTradeDao testRepo= new InMemTradeDao();
    	testTrade=testRepo.create(testTrade);
    	assertTrue(testRepo.findById(testTrade.getId()).equals(testTrade));
    }
    
    @Test
    public void test_saveAndGetAllTrades() 
    {
    	Trade[] testTradeArray=new Trade[5];
    	InMemTradeDao testRepo = new InMemTradeDao();

        for(int i=0; i<testTradeArray.length; ++i) {
        	testTradeArray[i] = new Trade(-1, testName, testPrice,testVolume);

            testRepo.create(testTradeArray[i]);
        }

        List<Trade> tradeResults = testRepo.findAll();
        LOG.info("Received [" + tradeResults.size() +
                 "] Trades from repository");

        for(Trade currentTrade: testTradeArray) {
            assertTrue(tradeResults.contains(currentTrade));
        }
        LOG.info("Matched [" + testTradeArray.length + "] Trades");
    }
    
    @Test (expected = TradeNotFoundException.class)
    public void test_deleteTrade() {
    	Trade[] testTradeArray = new Trade[100];
        InMemTradeDao testRepo = new InMemTradeDao();

        for(int i=0; i<testTradeArray.length; ++i) {
          	testTradeArray[i] = new Trade(-1, testName, testPrice,testVolume);

            testRepo.create(testTradeArray[i]);
        }
        Trade removedTrade = testTradeArray[5];
    	testRepo.deleteById(removedTrade.getId());
    	LOG.info("Removed item from trade array");
    	testRepo.findById(removedTrade.getId());
    }
    
}
