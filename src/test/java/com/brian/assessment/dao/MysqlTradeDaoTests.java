package com.brian.assessment.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.brian.assessment.exceptions.TradeNotFoundException;
import com.brian.assessment.model.Trade;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class MysqlTradeDaoTests {

	
	@Autowired
	MysqlTradeDao mysqlTradeDao;
	
	@Test
	@Transactional
	public void test_createAndFindAll() {
		mysqlTradeDao.create(new Trade(-1,"Test1",10.0,100));
		assertEquals(mysqlTradeDao.findAll().size(), 1);
	}
	@Test
	@Transactional
	public void test_createAndFindById() {
		int id=mysqlTradeDao.create(new Trade(-1,"Test1",10.0,100)).getId();
		mysqlTradeDao.create(new Trade(-1,"Test2",20.0,100));
		mysqlTradeDao.create(new Trade(-1,"Test3",30.0,100));
		assertEquals(mysqlTradeDao.findById(id).getId(), id);
	}
	@Test
	@Transactional
	public void test_createAndDelete() {
		mysqlTradeDao.create(new Trade(-1,"Test1",10.0,100));
		mysqlTradeDao.create(new Trade(-1,"Test1",10.0,100));
		mysqlTradeDao.create(new Trade(-1,"Test1",10.0,100));
		mysqlTradeDao.deleteById(2);
		assertEquals(mysqlTradeDao.findAll().size(), 2);
	}
	
	@Test (expected =TradeNotFoundException.class)
	public void test_createAndFindByIdFails() {
		mysqlTradeDao.findById(20);
	}
}
