package com.brian.assessment.rest;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.brian.assessment.model.Trade;
import com.brian.assessment.services.TradeService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
@AutoConfigureMockMvc
public class TradeControllerTests {

	private static final Logger logger = LoggerFactory.getLogger(TradeControllerTests.class);

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private TradeService mockTradeService;

	@Test
	public void findAllTrades_returnsList() throws Exception {
		when(mockTradeService.findAll()).thenReturn(new ArrayList<Trade>());

		MvcResult result = this.mockMvc.perform(get("/trades")).andExpect(status().isOk())
				.andExpect(jsonPath("$.size()").isNumber()).andReturn();

		logger.info("Result from TradeDao.findAll: " + result.getResponse().getContentAsString());
	}

	@Test
	public void createTrade_returnsCreated() throws Exception {
		Trade testTrade = new Trade(-1, "BRB", 137.37,500);

		this.mockMvc
				.perform(post("/trades").contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(testTrade)))
				.andExpect(status().isCreated()).andReturn();
		logger.info("Result from Create trade");
	}

	@Test
	public void deleteTrade_returnsOK() throws Exception {
		MvcResult result = this.mockMvc.perform(delete("/trades/5")).andExpect(status().isNoContent()).andReturn();

		logger.info("Result from TradeDao.delete: " + result.getResponse().getContentAsString());
	}
	//
	@Test
	public void getTradeById_returnsOK() throws Exception {//not working test, didn't create it it looks like
		Trade testTrade = new Trade(-1, "BRB2", 137.37,500);

		when(mockTradeService.findById(testTrade.getId())).thenReturn(testTrade);

		MvcResult result = this.mockMvc.perform(get("/trades/"+testTrade.getId())).andExpect(status().isOk()).andReturn();

		logger.info("Result from TradeDao.getTrade: " + result.getResponse().getContentAsString());
	}

}
